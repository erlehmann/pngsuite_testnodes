# PngSuite Testnodes

## Synopsis

This Minetest mod registers nodes with textures from the “official” test-suite for PNG created by Willem van Schaik.

## Usage

Place the nodes in the world. Compare the rendered results in Minetest with the files.

## References

The textures in this mod were downloaded from the PngSuite homepage:

* <http://www.schaik.com/pngsuite/>
