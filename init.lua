--[[

pngsuite_basic_formats_testnodes – Minetest mod that adds nodes with test images in all of the standard PNG b/w, color and paletted formats.
Copyright © 2023  Nils Dagsson Moskopp (erle)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 2.1 of the
License, or (at your option) any later version.

Dieses Programm hat das Ziel, die Medienkompetenz der Leser zu
steigern. Gelegentlich packe ich sogar einen handfesten Buffer
Overflow oder eine Format String Vulnerability zwischen die anderen
Codezeilen und schreibe das auch nicht dran.

]]--

local modname = minetest.get_current_modname()

local S = minetest.get_translator( modname )

local textures_path = minetest.get_modpath( modname ) .. "/textures/"

local textures = {
	["basn0g01"] = "basic black & white",
	["basn0g02"] = "basic 2 bit (4 level) grayscale",
	["basn0g04"] = "basic 4 bit (16 level) grayscale",
	["basn0g08"] = "basic 8 bit (256 level) grayscale",
	["basn0g16"] = "basic 16 bit (64k level) grayscale",
	["basn2c08"] = "basic 3x8 bits rgb color",
	["basn2c16"] = "basic 3x16 bits rgb color",
	["basn3p01"] = "basic 1 bit (2 color) paletted",
	["basn3p02"] = "basic 2 bit (4 color) paletted",
	["basn3p04"] = "basic 4 bit (16 color) paletted",
	["basn3p08"] = "basic 8 bit (256 color) paletted",
	["basn4a08"] = "basic 8 bit grayscale + 8 bit alpha-channel",
	["basn4a16"] = "basic 16 bit grayscale + 16 bit alpha-channel",
	["basn6a08"] = "basic 3x8 bits rgb color + 8 bit alpha-channel",
	["basn6a16"] = "basic x16 bits rgb color + 16 bit alpha-channel",
}

for prefix, description in pairs(textures) do
	local nodename = modname .. ":" .. prefix
	local filename = prefix .. ".png"
	minetest.register_node(nodename, {
		description = S(
			"PngSuite Test Node " ..
			prefix ..
			" – " ..
			description
		),
		drawtype = "glasslike",
		groups = { dig_immediate = 2 },
		paramtype = "light",
		sunlight_propagates = true,
		tiles = { filename },
		use_texture_alpha = "blend",
	})
end
